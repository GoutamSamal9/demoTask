import axios from 'axios';

export const getUserList = async () => {
    const response = await axios.get(
        `https://reqres.in/api/users`);
    const { data } = response;
    return data;
};


export const postDetails = async (details) => {
    const response = await axios.post(
        `https://reqres.in/api/users`, details);
    const { data } = response;
    return data;
};


export const editDetails = async (details) => {
    const response = await axios.patch(
        `https://reqres.in/api/users`, details);
    const { data } = response;
    return data;
};