
import './App.css';
import UserDetails from './Components/UserDetails';
import { useStore } from 'laco-react';
import { useEffect } from 'react';
import { UserStore } from './UserStore';
import { getUserList } from './EndPoint';
import { Grid, makeStyles } from '@material-ui/core';
import AddFrom from './Components/AddFrom';


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1200,
    margin: 'auto'
  },

}));

function App() {

  const classes = useStyles();

  const { userList } = useStore(UserStore);

  const getUserData = () => {
    getUserList().then((response) => {
      UserStore.set(() => ({
        userList: { data: [...userList.data, ...response.data] }
      }),
        'UserStore-data loader'
      );
    }).catch((error) => {
      console.log(error);
    })
  }

  useEffect(() => {
    if (!userList.data.length) getUserData();
  })

  return (
    <div className={classes.root}>
      <AddFrom />
      <Grid container spacing={6}>
        {
          userList.data.map((each, i) => (
            <Grid item lg={4} key={i + 1} >
              <UserDetails each={each} />
            </Grid>
          ))
        }

      </Grid>


    </div>
  );
}

export default App;
