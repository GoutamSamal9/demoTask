import { Store } from 'laco';

export const UserStore = new Store({
    userList: { data: [] },
    isDialogOpen: false,
    userObj: {
        id: '',
        email: '',
        first_name: '',
        last_name: '',
    },

}, 'UserStore');