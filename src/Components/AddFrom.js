import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import AddIcon from '@material-ui/icons/Add';
import DialogTitle from '@material-ui/core/DialogTitle';
import { IconButton, makeStyles, TextField } from '@material-ui/core';
import { useStore } from 'laco-react';
import { UserStore } from '../UserStore';
import { editDetails, postDetails } from '../EndPoint';

const useStyles = makeStyles(() => ({
    addIcon: {
        position: 'fixed',
        bottom: 30,
        right: 30,
    }
}))



export default function AddFrom() {
    const classes = useStyles();

    const { isDialogOpen, userObj, userList } = useStore(UserStore);



    const updateValue = (key, value) => {
        UserStore.set(
            () => ({ userObj: { ...userObj, [key]: value } }),
            'userObj-onchange'
        );
    };

    const handleClickOpen = () => {
        UserStore.set(() => ({
            isDialogOpen: true
        }),
            'UserStore-data loader'
        );
    };

    const handleClose = () => {
        UserStore.set(() => ({
            isDialogOpen: false,
            userObj: {
                ...userObj,
                email: '',
                first_name: '',
                last_name: '',
            },
        }),
            'UserStore-data loader'
        );
    };

    const handelAddDetails = () => {
        if (userObj.id) {
            editDetails(userObj, userObj.id).then((response) => {
                console.log(response);
                let index = userList.data.findIndex(item => item._id === userObj.id);
                let list = [...userList.data];
                list.splice(index, 1, response);
                UserStore.set(() => ({ userList: { data: list } }));
                handleClose();
            })
        } else {
            postDetails(userObj).then((response) => {
                UserStore.set(() => ({
                    userList: { data: [...userList.data, response] }
                }),
                    'UserStore-data loader'
                );
                handleClose();
                alert("Created")
            })
        }

    }

    return (
        <div>

            <IconButton component="span" onClick={handleClickOpen} color="secondary" className={classes.addIcon} >
                <AddIcon size="large" color="secondary" style={{ width: 50, height: 50 }} />
            </IconButton>
            <Dialog
                open={isDialogOpen}
                onClose={handleClose}
            >
                <DialogTitle >Add Your Details</DialogTitle>

                <DialogContent>

                    <TextField
                        label="email"
                        fullWidth
                        id="outlined-margin-dense"
                        className={classes.textField}
                        type="mail"
                        required
                        margin="dense"
                        variant="outlined"
                        onChange={(event) => updateValue('email', event.target.value)}
                        value={userObj.email}
                    />
                    <TextField
                        label="First Name"
                        fullWidth
                        required
                        id="outlined-margin-dense"
                        className={classes.textField}
                        margin="dense"
                        variant="outlined"
                        onChange={(event) => updateValue('first_name', event.target.value)}
                        value={userObj.first_name}
                    />

                    <TextField
                        label="Last Name"
                        fullWidth
                        required
                        id="outlined-margin-dense"
                        className={classes.textField}
                        margin="dense"
                        variant="outlined"
                        onChange={(event) => updateValue('last_name', event.target.value)}
                        value={userObj.last_name}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={handelAddDetails} color="primary" autoFocus>
                        Add
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
