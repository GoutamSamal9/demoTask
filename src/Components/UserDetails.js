import { Card, CardActions, CardContent, CardMedia, IconButton, Link, makeStyles, Typography } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
import { useStore } from 'laco-react';
import { UserStore } from '../UserStore';

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 300,
        width: '100%',
        maxHeight: 300,
        marginTop: theme.spacing(10)
    },
    media: {
        maxHeight: 200,
        objectFit: 'content'
    },

    iconDelete: {
        marginLeft: 'auto'
    },

}))

function UserDetails({ each }) {

    const classes = useStyles();
    const { userList } = useStore(UserStore);

    const handelDelete = (eachIid) => {
        let index = userList.data.findIndex(item => item.id === eachIid);
        let list = [...userList.data];
        list.splice(index, 1);
        UserStore.set(() => ({ userList: { data: list } }));

    }


    const handelEdit = (each) => {
        console.log(each);
        UserStore.set(() => ({
            isDialogOpen: true,
            userObj: {
                id: each.id,
                email: each.email,
                first_name: each.first_name,
                last_name: each.last_name,
            }
        }),
            'UserStore-data loader'
        );
    }

    return (
        <div className={classes.root}>
            <Card>
                <CardMedia
                    component="img"
                    className={classes.media}
                    image={each.avatar}
                    title="demo" />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {each.first_name} {each.last_name}
                    </Typography>
                    <Typography component={Link} href={`mailto:${each.email}`}  >
                        {each.email}
                    </Typography>
                </CardContent>
                <CardActions disableSpacing>
                    <IconButton color="secondary" onClick={() => handelDelete(each.id)} className={classes.iconDelete} >
                        <Delete />
                    </IconButton>
                    <IconButton color="secondary" onClick={() => handelEdit(each)} className={classes.edit} >
                        <Edit />
                    </IconButton>
                </CardActions>
            </Card>
        </div>
    );
}
export default UserDetails;